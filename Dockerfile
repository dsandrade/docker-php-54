FROM alpine:3.4

MAINTAINER "Dsandrade" <danielsanto.andrade@hotmail.com>

#
#--------------------------------------------------------------------------
# Enviroments Variables
#--------------------------------------------------------------------------
#
ENV PHPIZE_DEPS \
		autoconf \
		file \
		g++ \
		gcc \
		libc-dev \
		make \
		pkgconf \
		re2c

ENV PHP_INI_DIR /usr/local/etc/php

ENV PHP_CFLAGS="-fstack-protector-strong -fpic -fpie -O2"
ENV PHP_CPPFLAGS="$PHP_CFLAGS"
ENV PHP_LDFLAGS="-Wl,-O1 -Wl,--hash-style=both -pie"
ENV PHP_EXTRA_CONFIGURE_ARGS --with-apxs2

#
#--------------------------------------------------------------------------
# Install Apache
#--------------------------------------------------------------------------
#

RUN apk add --no-cache --virtual .persistent-deps \
		ca-certificates \
		openldap-dev \
		curl \
		tar \
		apache2 \
		gettext \
		bash \
		xz

#
#--------------------------------------------------------------------------
# Install PHP
#--------------------------------------------------------------------------
#

RUN mkdir -p $PHP_INI_DIR/conf.d

ENV GPG_KEYS F38252826ACD957EF380D39F2F7956BC5DA04B5D

ENV PHP_VERSION 5.4.45
ENV PHP_URL="https://secure.php.net/get/php-5.4.45.tar.gz/from/this/mirror"
ENV PHP_ASC_URL="https://secure.php.net/get/php-5.4.45.tar.gz.asc/from/this/mirror"
ENV PHP_SHA256="" PHP_MD5="ba580e774ed1ab256f22d1fa69a59311"

RUN set -xe; \
	\
	apk add --no-cache --virtual .fetch-deps \
		gnupg \
		openssl \
	; \
	\
	mkdir -p /usr/src; \
	cd /usr/src; \
	\
	wget -O php.tar.gz "$PHP_URL"; \
	\
	if [ -n "$PHP_SHA256" ]; then \
		echo "$PHP_SHA256 *php.tar.gz" | sha256sum -c -; \
	fi; \
	if [ -n "$PHP_MD5" ]; then \
		echo "$PHP_MD5 *php.tar.gz" | md5sum -c -; \
	fi; \
	\
	if [ -n "$PHP_ASC_URL" ]; then \
		wget -O php.tar.gz.asc "$PHP_ASC_URL"; \
		export GNUPGHOME="$(mktemp -d)"; \
		for key in $GPG_KEYS; do \
			gpg --keyserver ha.pool.sks-keyservers.net --recv-keys "$key"; \
		done; \
		gpg --batch --verify php.tar.gz.asc php.tar.gz; \
		rm -r "$GNUPGHOME"; \
	fi; \
	\
	apk del .fetch-deps

#
#--------------------------------------------------------------------------
# Configure Apache and PHP
#--------------------------------------------------------------------------
#

COPY ./storage/docker/docker-php-source /usr/local/bin/

RUN set -xe \
	&& apk add --no-cache --virtual .build-deps \
		$PHPIZE_DEPS \
		curl-dev \
		libedit-dev \
		libxml2-dev \
		openssl-dev \
		sqlite-dev \
		apache2-dev \
	\
	&& export CFLAGS="$PHP_CFLAGS" \
		CPPFLAGS="$PHP_CPPFLAGS" \
		LDFLAGS="$PHP_LDFLAGS" \
	&& docker-php-source extract \
	&& cd /usr/src/php \
	&& ./configure \
		--with-config-file-path="$PHP_INI_DIR" \
		--with-config-file-scan-dir="$PHP_INI_DIR/conf.d" \
		\
		--disable-cgi \
		\
		--enable-ftp \
		--enable-mbstring \
		--enable-mysqlnd \
		\
		--with-curl \
		--with-libedit \
		--with-openssl \
		--with-zlib \
		\
		$PHP_EXTRA_CONFIGURE_ARGS \
	&& make -j "$(getconf _NPROCESSORS_ONLN)" \
	&& make install \
	&& { find /usr/local/bin /usr/local/sbin -type f -perm +0111 -exec strip --strip-all '{}' + || true; } \
	&& make clean \
	&& docker-php-source delete \
	\
	&& runDeps="$( \
		scanelf --needed --nobanner --recursive /usr/local \
			| awk '{ gsub(/,/, "\nso:", $2); print "so:" $2 }' \
			| sort -u \
			| xargs -r apk info --installed \
			| sort -u \
	)" \
	&& apk add --no-cache --virtual .php-rundeps $runDeps \
	\
	&& apk del .build-deps


ENV APACHE_CONFDIR /etc/apache2
ENV APACHE_ENVVARS $APACHE_CONFDIR/envvars

COPY ./storage/docker/envvars $APACHE_ENVVARS

RUN set -ex \
	\
	&& sed -ri 's/^export ([^=]+)=(.*)$/: ${\1:=\2}\nexport \1/' "$APACHE_ENVVARS" \
	\
# setup directories and permissions
	&& . "$APACHE_ENVVARS" \
	&& for dir in \
		"$APACHE_LOCK_DIR" \
		"$APACHE_RUN_DIR" \
		"$APACHE_LOG_DIR" \
		/var/www/html \
		/usr/share/wwww \
	; do \
		rm -rvf "$dir" \
		&& mkdir -p "$dir" \
		&& chown -R "$APACHE_RUN_USER:$APACHE_RUN_GROUP" "$dir"; \
	done \
	&& rm -fr /var/www/localhost

# logs should go to stdout / stderr
RUN set -ex \
	&& . "$APACHE_ENVVARS" \
	&& ln -sfT /dev/stderr "$APACHE_LOG_DIR/error.log" \
	&& ln -sfT /dev/stdout "$APACHE_LOG_DIR/access.log" \
	&& ln -sfT /dev/stdout "$APACHE_LOG_DIR/other_vhosts_access.log"

# PHP files should be handled by PHP, and should be preferred over any other file type
RUN { \
		echo -e '<FilesMatch \.php$>'; \
		echo -e '\tSetHandler application/x-httpd-php'; \
		echo -e '</FilesMatch>'; \
		echo; \
		echo -e 'DirectoryIndex disabled'; \
		echo -e 'DirectoryIndex index.php index.html'; \
		echo;  \
		echo -e '<Directory /var/www/>'; \
		echo -e '\tOptions -Indexes'; \
		echo -e '\tAllowOverride All'; \
		echo -e '</Directory>'; \
	} | tee "$APACHE_CONFDIR/conf.d/php.conf"

# Fix DocumentRoot and PHP module loading
RUN sed -i 's@/var/www/localhost/htdocs@/usr/share/www@' /etc/apache2/httpd.conf \
	&& sed -i 's@LoadModule php5_module        lib/apache2/libphp5.so@LoadModule php5_module        modules/libphp5.so@'  /etc/apache2/httpd.conf

COPY ./storage/docker/docker-php-ext-* /usr/local/bin/

RUN docker-php-ext-install mysqli
RUN apk upgrade --update && apk add \
  coreutils \
  freetype-dev \
  libjpeg-turbo-dev \
  libltdl \
  libmcrypt-dev \
  libpng-dev \
&& docker-php-ext-configure ldap --with-libdir=lib/ \
&& docker-php-ext-install ldap \
&& docker-php-ext-install -j$(nproc) iconv mcrypt \
&& docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
&& docker-php-ext-install -j$(nproc) gd

#
#--------------------------------------------------------------------------
# User
#--------------------------------------------------------------------------
#
ARG DOCKER_USER=docker
ENV DOCKER_USER ${DOCKER_USER}

RUN addgroup -g 1000 ${DOCKER_USER} && \
    adduser -u 1000 -h /home/${DOCKER_USER} -D -G ${DOCKER_USER} ${DOCKER_USER} && \
    chown -R ${DOCKER_USER}:${DOCKER_USER} /var/www/html && \
    chown -R ${DOCKER_USER}:${DOCKER_USER} /home/${DOCKER_USER} && \
    sed -i "s/apache:x:101:apache/apache:x:101:apache,${DOCKER_USER}/" /etc/group

RUN chown -R ${DOCKER_USER}:${DOCKER_USER} /home/${DOCKER_USER}

COPY ./storage/docker/docker-php-entrypoint /usr/local/bin/

COPY ./storage/docker/apache2-foreground /usr/local/bin/
COPY ./storage/docker/httpd.conf /usr/local/etc/httpd.conf.tpl

RUN mkdir -p /etc/apache2/vhosts

COPY ./storage/docker/vhost.conf /etc/apache2/vhosts/

WORKDIR /var/www/html

EXPOSE 80

CMD ["apache2-foreground"]